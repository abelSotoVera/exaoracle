create or replace PACKAGE METRIC_PROCESS_PKG AS 

  /* Author: Abel Rolando Soto Vera  */ 
  
  function get_output1(p_school_id IN T_SCHOOL.school_id%TYPE,
                       p_start_date IN DATE,
                       p_end_date IN DATE,
                       p_last_name IN T_STUDENT.last_name%TYPE,
                       p_subject_id IN T_SUBJECT.subject_id%TYPE) RETURN out1_tb;

  function get_output2(p_school_id IN T_SCHOOL.school_id%TYPE,
                       p_start_date IN DATE,
                       p_end_date IN DATE,
                       p_last_name IN T_STUDENT.last_name%TYPE,
                       p_subject_id IN T_SUBJECT.subject_id%TYPE) RETURN out2_tb;

  --Main procedure
  PROCEDURE main_process(p_school_id IN T_SCHOOL.school_id%TYPE,
                       p_start_date IN DATE,
                       p_end_date IN DATE,
                       p_last_name IN T_STUDENT.last_name%TYPE,
                       p_subject_id IN T_SUBJECT.subject_id%TYPE,
                       p_out1_tb OUT out1_tb,
                       p_out2_tb OUT out2_tb) ;
                       

END METRIC_PROCESS_PKG;