CREATE SEQUENCE s_school_id;

CREATE TABLE t_school(school_id NUMBER(14) DEFAULT ON NULL s_school_id.NEXTVAL NOT NULL,
                      school_name VARCHAR2(500) NOT NULL,
                      postal_code VARCHAR2(12) NOT NULL,
                      CONSTRAINT c_school_pk PRIMARY KEY(school_id)
                     );
                     
CREATE UNIQUE INDEX i_school_uc ON t_school(school_name,postal_code); 

CREATE SEQUENCE s_subject_id;

CREATE TABLE t_subject(subject_id NUMBER(14) DEFAULT ON NULL s_subject_id.NEXTVAL,
                       subject_name VARCHAR2(100) NOT NULL,
                       CONSTRAINT c_subject_pk PRIMARY KEY(subject_id)
                      );

CREATE SEQUENCE student_id_seq;

CREATE TABLE t_student(student_id NUMBER(14) DEFAULT ON NULL student_id_seq.NEXTVAL NOT NULL,
                       school_id NUMBER(14) NOT NULL,
                       first_name VARCHAR2(100) NOT NULL,
                       last_name VARCHAR2(100) NOT NULL,
                       date_of_birth DATE NOT NULL,
                       CONSTRAINT c_student_pk PRIMARY KEY(student_id),
                       CONSTRAINT c_student_school_fk FOREIGN KEY(school_id) REFERENCES t_school(school_id));

CREATE INDEX i_student_school_id ON t_student(school_id);
CREATE INDEX i_student_fname_upper ON t_student(UPPER(first_name));
CREATE INDEX i_student_lname_upper ON t_student(UPPER(last_name));

CREATE TABLE t_absence_reason (absence_code VARCHAR2(3) NOT NULL,
                                absence_reason VARCHAR2(100) NOT NULL,
                                CONSTRAINT c_absence_pk PRIMARY KEY(absence_code));
                                
CREATE TABLE t_performance_metric(metric_code VARCHAR2(3) NOT NULL,
                                  metric_desc VARCHAR2(100) NOT NULL,
                                  CONSTRAINT c_metric_code PRIMARY KEY(metric_code));



CREATE TABLE t_attendance_register(student_id NUMBER(14) NOT NULL,
                                   school_day DATE NOT NULL,
                                   present_yn VARCHAR2(1) NOT NULL,
                                   absence_code VARCHAR2(3),
                                   CONSTRAINT c_attend_pk PRIMARY KEY(student_id,school_day),
                                   CONSTRAINT c_attend_child_fk FOREIGN KEY(student_id) REFERENCES t_student(student_id),
                                   CONSTRAINT c_attend_absence_code_fk FOREIGN KEY(absence_code) REFERENCES t_absence_reason(absence_code),
                                   CONSTRAINT c_attend_present_yn_chk CHECK (present_yn IN('Y','N'))
                                  );
                                  
CREATE INDEX i_attend_absence_code ON t_attendance_register(absence_code);

CREATE SEQUENCE s_lesson_id; 
                                 
CREATE TABLE t_lesson(lesson_id NUMBER(14) DEFAULT ON NULL s_lesson_id.NEXTVAL NOT NULL,
                      student_id NUMBER(14) NOT NULL,
                      school_day DATE NOT NULL,
                      subject_id NUMBER(14) NOT NULL,    
                      present_yn VARCHAR2(1) NOT NULL,
                      absence_code VARCHAR2(3),
                      CONSTRAINT c_lesson_pk PRIMARY KEY(lesson_id),
                      CONSTRAINT c_lesson_uc UNIQUE(student_id,school_day,subject_id) USING INDEX,
                      CONSTRAINT c_lesson_student_fk FOREIGN KEY(student_id) REFERENCES t_student(student_id),
                      CONSTRAINT c_lesson_subject_fk FOREIGN KEY(subject_id) REFERENCES t_subject(subject_id),
                      CONSTRAINT c_lesson_absence_code_fk FOREIGN KEY(absence_code) REFERENCES t_absence_reason(absence_code),
                      CONSTRAINT c_lesson_present_yn_chk CHECK (present_yn IN('Y','N'))
                     );
                     
CREATE INDEX i_lesson_subject ON t_lesson(subject_id);
CREATE INDEX i_lesson_absence_code ON t_lesson(absence_code);
                     
CREATE TABLE t_lesson_metric(lesson_id NUMBER(14) NOT NULL,
                             metric_code VARCHAR2(3) NOT NULL,
                             metric_score NUMBER(2) NOT NULL,
                             CONSTRAINT c_lesson_met_pk PRIMARY KEY(lesson_id,metric_code),
                             CONSTRAINT c_lesson_metric_code_fk FOREIGN KEY(metric_code) REFERENCES t_performance_metric(metric_code)
                            );

CREATE INDEX i_lesson_metric_code ON t_lesson_metric(metric_code);

INSERT INTO t_school(school_name,postal_code)
VALUES('Barnet Academy','ZX64 R1D');

INSERT INTO t_school(school_name,postal_code)
VALUES('Dibbingsdale Secondary Modern','PQ12 A1X');

INSERT INTO t_subject(subject_name) VALUES('English');
INSERT INTO t_subject(subject_name) VALUES('Mathematics');
INSERT INTO t_subject(subject_name) VALUES('French');
INSERT INTO t_subject(subject_name) VALUES('Latin');
INSERT INTO t_subject(subject_name) VALUES('Biology');
INSERT INTO t_subject(subject_name) VALUES('Chemistry');
INSERT INTO t_subject(subject_name) VALUES('Physics');
INSERT INTO t_subject(subject_name) VALUES('Design and Technology');
INSERT INTO t_subject(subject_name) VALUES('Geography');
INSERT INTO t_subject(subject_name) VALUES('History');

INSERT INTO t_absence_reason(absence_code,absence_reason)
VALUES('ILL','Illness');

INSERT INTO t_absence_reason(absence_code,absence_reason)
VALUES('DEN','Dentist');

INSERT INTO t_absence_reason(absence_code,absence_reason)
VALUES('FAM','Family Occasion');

INSERT INTO t_performance_metric(metric_code,metric_desc)
VALUES('E','Effort');

INSERT INTO t_performance_metric(metric_code,metric_desc)
VALUES('A','Attainment');

COMMIT;



