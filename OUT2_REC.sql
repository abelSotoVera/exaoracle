create or replace TYPE out2_rec AS OBJECT
(
      school_name VARCHAR2(500),
      first_name VARCHAR2(100),
      last_name  VARCHAR2(100),
      school_day DATE,
      subject_name VARCHAR2(100),
      percentage_ch VARCHAR2(300)
)
;