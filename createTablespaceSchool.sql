create tablespace 
   TBS_SCHOOL_DATA
datafile   
  '/u01/app/oracle/oradata/xe/TBS_SCHOOL_DATA.dbf' 
size 50m
autoextend on;
create tablespace 
   TBS_SCHOOL_IDX
datafile   
  '/u01/app/oracle/oradata/xe/TBS_SCHOOL_IDX.dbf' 
size 50m
autoextend on;