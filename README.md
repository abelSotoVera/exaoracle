## exaOracle
#I attached some files that helped me to solved the test.

#To set up an environment to start to work, I used a some scripts to create a tablespace and school schema On #Oracle 12c  (I used docker to solved, because Currently I am using a Macbook :) 

#createTablespaceSchool.sql
#createUserSchool.sql

#Related with the tasks see the description below:

#Task #1 : This task is in file Task1.sql

#Task #2 : This task is in file Task2.sql

#Task #3 : This task is in file Task3.sql

#For the task #4 I created some oracle objects.
#    Type collections:
#        OUT1_REC.sql
#        OUT1_TB.sql
#        OUT2_REC.sql
#        OUT2_TB.sql
#    Package (spec and body)
#       METRIC_PROCESS_PKG.pks
#       METRIC_PROCESS_PKG.pkb
#You can execute this block for the task # 4;

#DECLARE
#  P_SCHOOL_ID NUMBER;
#  P_START_DATE DATE;
#  P_END_DATE DATE;
#  P_LAST_NAME VARCHAR2(100);
#  P_SUBJECT_ID NUMBER;
#  P_OUT1_TB SCHOOL.OUT1_TB;
#  P_OUT2_TB SCHOOL.OUT2_TB;
#BEGIN
#  P_SCHOOL_ID := NULL;
#  P_START_DATE := NULL;
#  P_END_DATE := NULL;
#  P_LAST_NAME := NULL;
#  P_SUBJECT_ID := NULL;
#
#  METRIC_PROCESS_PKG.MAIN_PROCESS(
#    P_SCHOOL_ID => P_SCHOOL_ID,
#    P_START_DATE => P_START_DATE,
#    P_END_DATE => P_END_DATE,
#    P_LAST_NAME => P_LAST_NAME,
#    P_SUBJECT_ID => P_SUBJECT_ID,
#    P_OUT1_TB => P_OUT1_TB,
#    P_OUT2_TB => P_OUT2_TB
#  );
#  /* Legacy output: 
#DBMS_OUTPUT.PUT_LINE('P_OUT1_TB = ' || P_OUT1_TB);
#*/ 
#  --:P_OUT1_TB := P_OUT1_TB;
#  /* Legacy output: 
#DBMS_OUTPUT.PUT_LINE('P_OUT2_TB = ' || P_OUT2_TB);
#*/ 
#  --:P_OUT2_TB := P_OUT2_TB;
#--rollback; 
#END;


#Also, I attached a doc file with some screenshot of the process#.