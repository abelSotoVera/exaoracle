create or replace PACKAGE BODY METRIC_PROCESS_PKG AS 

  v_out1_rec out1_rec;
  v_out2_rec out2_rec;
  
  /* Author: Abel Rolando Soto Vera  */ 
  FUNCTION get_output1(p_school_id IN T_SCHOOL.school_id%TYPE,
                       p_start_date IN DATE,
                       p_end_date IN DATE,
                       p_last_name IN T_STUDENT.last_name%TYPE,
                       p_subject_id IN T_SUBJECT.subject_id%TYPE) RETURN out1_tb IS
  CURSOR get_data(cp_school_id IN T_SCHOOL.school_id%TYPE,
                  cp_start_date IN DATE,
                  cp_end_date IN DATE,
                  cp_last_name IN T_STUDENT.last_name%TYPE,
                  cp_subject_id IN T_SUBJECT.subject_id%TYPE) IS
  SELECT SC.SCHOOL_NAME, ST.FIRST_NAME, ST.LAST_NAME, LES.SCHOOL_DAY , SBJ.SUBJECT_NAME
       , PME.METRIC_DESC, LME.METRIC_SCORE
       , ROUND(AVG(LME.METRIC_SCORE) OVER (PARTITION BY PME.METRIC_CODE ORDER BY SC.SCHOOL_NAME, ST.FIRST_NAME, ST.LAST_NAME, LES.SCHOOL_DAY , SBJ.SUBJECT_NAME ), 2) avg_score
  FROM T_SCHOOL SC, T_STUDENT ST, T_LESSON LES, T_SUBJECT SBJ
      , T_LESSON_METRIC LME, T_PERFORMANCE_METRIC PME
 WHERE SC.SCHOOL_ID = ST.SCHOOL_ID
   AND ST.STUDENT_ID = LES.STUDENT_ID
   AND SBJ.SUBJECT_ID = LES.SUBJECT_ID
   AND LME.LESSON_ID = LES.LESSON_ID
   AND LME.METRIC_CODE = PME.METRIC_CODE
   AND SC.SCHOOL_ID = NVL(cp_school_id, SC.SCHOOL_ID)
   AND LES.SCHOOL_DAY BETWEEN cp_start_date AND cp_end_date
   AND UPPER(ST.LAST_NAME) = NVL(cp_last_name,ST.LAST_NAME)
   AND SBJ.SUBJECT_ID IN (cp_subject_id)
   
   ORDER BY 1, 3, 4, SBJ.SUBJECT_ID
   ;
  v_out1_tb out1_tb := out1_tb();
  BEGIN
    FOR rec IN get_data(p_school_id,p_start_date, p_end_date, p_last_name, p_subject_id) LOOP
        DBMS_OUTPUT.PUT_LINE (rec.last_name || '' || rec.avg_score);
       v_out1_tb.EXTEND;
       v_out1_rec.school_name := rec.school_name;
       v_out1_rec.first_name := rec.first_name;
       v_out1_rec.last_name := rec.last_name;
       v_out1_rec.school_day := rec.school_day;
       v_out1_rec.subject_name := rec.subject_name;
       v_out1_rec.metric_desc := rec.metric_desc;
       v_out1_rec.metric_score := rec.metric_score;
       v_out1_rec.avg_score := rec.avg_score;
       
       v_out1_tb(v_out1_tb.last):=v_out1_rec;
    END LOOP;
    
    RETURN v_out1_tb;
  END get_output1;


  FUNCTION get_output2(p_school_id IN T_SCHOOL.school_id%TYPE,
                       p_start_date IN DATE,
                       p_end_date IN DATE,
                       p_last_name IN T_STUDENT.last_name%TYPE,
                       p_subject_id IN T_SUBJECT.subject_id%TYPE) RETURN out2_tb IS
  
  CURSOR get_data(cp_school_id IN T_SCHOOL.school_id%TYPE,
                  cp_start_date IN DATE,
                  cp_end_date IN DATE,
                  cp_last_name IN T_STUDENT.last_name%TYPE,
                  cp_subject_id IN T_SUBJECT.subject_id%TYPE) IS
  SELECT SC.SCHOOL_NAME, ST.FIRST_NAME, ST.LAST_NAME, LES.SCHOOL_DAY , SBJ.SUBJECT_NAME
       , SUM(LME.METRIC_SCORE) sum_score
   FROM T_SCHOOL SC, T_STUDENT ST, T_LESSON LES, T_SUBJECT SBJ
      , T_LESSON_METRIC LME, T_PERFORMANCE_METRIC PME
 WHERE SC.SCHOOL_ID = ST.SCHOOL_ID
   AND ST.STUDENT_ID = LES.STUDENT_ID
   AND SBJ.SUBJECT_ID = LES.SUBJECT_ID
   AND LME.LESSON_ID = LES.LESSON_ID
   AND LME.METRIC_CODE = PME.METRIC_CODE
   AND SC.SCHOOL_ID = NVL(cp_school_id, SC.SCHOOL_ID)
   AND LES.SCHOOL_DAY BETWEEN cp_start_date AND cp_end_date
   AND ST.LAST_NAME = NVL(cp_last_name,ST.FIRST_NAME)
   AND SBJ.SUBJECT_ID IN (cp_subject_id)
  GROUP BY SC.SCHOOL_NAME, ST.FIRST_NAME, ST.LAST_NAME, LES.SCHOOL_DAY , SBJ.SUBJECT_NAME 
   ORDER BY 1, 3, 4,5
   ;
   
     v_out2_tb out2_tb := out2_tb();
     v_percentage NUMBER(10,2) :=0;
     v_sum_score_before NUMBER(10,2) :=0;
  BEGIN
    FOR rec IN get_data(p_school_id,p_start_date, p_end_date, p_last_name, p_subject_id) LOOP
        DBMS_OUTPUT.PUT_LINE (rec.last_name || '' || rec.sum_score);
       v_out2_tb.extend;
       v_out2_rec.school_name := rec.school_name;
       v_out2_rec.first_name := rec.first_name;
       v_out2_rec.last_name := rec.last_name;
       v_out2_rec.school_day := rec.school_day;
       v_out1_rec.subject_name := rec.subject_name;
       IF v_sum_score_before = 0 THEN
         v_sum_score_before := rec.sum_score;
         v_out2_rec.percentage_ch := '0 – This is the first day so we expect it to be 0';
       ELSE
         v_percentage := ROUND((1 - (rec.sum_score / v_sum_score_before ))*100, 2);
         v_out2_rec.percentage_ch := v_percentage||' - A score of '||rec.sum_score||' is a '
         ||v_percentage||'% drop in performance from the previous days score of'||v_sum_score_before ;
       
       END IF;
       
       v_out2_tb(v_out2_tb.last) := v_out2_rec;
    END LOOP;
    
    RETURN v_out2_tb;
  END get_output2;

  --Main procedure
  /*
  This process needs improvement to support the IN p_subject_in = '1,8,9' (e.g.)
  */
  PROCEDURE main_process(p_school_id IN T_SCHOOL.school_id%TYPE,
                       p_start_date IN DATE,
                       p_end_date IN DATE,
                       p_last_name IN T_STUDENT.last_name%TYPE,
                       p_subject_id IN T_SUBJECT.subject_id%TYPE,
                       p_out1_tb OUT out1_tb,
                       p_out2_tb OUT out2_tb) IS
  
  BEGIN
   p_out1_tb := METRIC_PROCESS_PKG.GET_OUTPUT1( P_SCHOOL_ID,
                                    P_START_DATE,
                                    P_END_DATE,
                                    P_LAST_NAME,
                                    P_SUBJECT_ID);
  
    p_out2_tb := METRIC_PROCESS_PKG.GET_OUTPUT2( P_SCHOOL_ID,
                                    P_START_DATE,
                                    P_END_DATE,
                                    P_LAST_NAME,
                                    P_SUBJECT_ID);   
  END main_process;

END METRIC_PROCESS_PKG;