create or replace TYPE out1_rec AS OBJECT
(
      school_name VARCHAR2(500),
      first_name VARCHAR2(100),
      last_name  VARCHAR2(100),
      school_day DATE,
      subject_name VARCHAR2(100),
      metric_desc VARCHAR2(100),
      metric_score NUMBER(2,0),
      avg_score NUMBER(10,2)
)