
DECLARE
  CURSOR cur_att_register IS
    SELECT * FROM T_ATTENDANCE_REGISTER
    WHERE student_id != 1006
    ;

  CURSOR get_subjects(cp_num NUMBER) IS
    SELECT * 
      FROM (SELECT * FROM t_subject
             ORDER BY dbms_random.value)
    WHERE rownum <= cp_num;

  v_num_lesson NUMBER(2);
BEGIN

  FOR rec IN cur_att_register LOOP
    v_num_lesson := trunc(DBMS_RANDOM.VALUE(5,8)); -- with trunc command the range is 5 - 7
    
    FOR idx IN get_subjects(v_num_lesson) LOOP
      INSERT INTO t_lesson (lesson_id, student_id,school_day,subject_id,present_yn,absence_code) 
      VALUES (S_LESSON_ID.NEXTVAL, rec.student_id, rec.school_day,idx.subject_id , rec.present_yn, rec.absence_code);
      --DBMS_OUTPUT.PUT_LINE(''|| idx.subject_id);
    END LOOP;
  END LOOP;
  COMMIT;

END;