DECLARE
  
  CURSOR cur_perf_metric IS
     SELECT *
       FROM T_PERFORMANCE_METRIC;
       
  CURSOR cur_lesson IS
    SELECT *
      FROM t_lesson
     WHERE PRESENT_YN = 'Y'
     --and rownum <= 3405
     ;  

  TYPE tb_lesson IS TABLE OF cur_lesson%ROWTYPE index by binary_integer;
  blk_lesson tb_lesson;
  
  v_limit NUMBER(5) := 100;
  v_metric_score T_LESSON_METRIC.METRIC_CODE%TYPE;

BEGIN
  OPEN cur_lesson;
  LOOP
    FETCH cur_lesson BULK COLLECT INTO blk_lesson LIMIT v_limit;
      FOR idx IN 1..blk_lesson.COUNT LOOP
        --DBMS_OUTPUT.PUT_LINE('11' ||blk_lesson(idx).subject_id);
        FOR rec IN cur_perf_metric LOOP
          v_metric_score := trunc(DBMS_RANDOM.VALUE(1,6)); -- with trunc command the range is 1 - 5
          INSERT INTO t_lesson_metric (lesson_id,metric_code,metric_score ) 
                VALUES (blk_lesson(idx).lesson_id,rec.metric_code, v_metric_score); 
        END LOOP;
      END LOOP;
      COMMIT;
      DBMS_OUTPUT.PUT_LINE('limite ' ||blk_lesson.count);
    EXIT WHEN cur_lesson%notfound;
  END LOOP;
  COMMIT;
  CLOSE cur_lesson;

END;
