/* Task1
  Date of birth in t_student has been defaulted to 01-Jan-1900 for every student.
  Please update this with a random date of birth between 1st of January 2002 and the 31st of December 2015. 
Abel Soto Vera 
*/
DECLARE
  
  CURSOR cur_stundent IS
    SELECT *
      FROM T_STUDENT
    FOR UPDATE  ;
  v_start_date DATE:= TO_DATE('01-01-2002','MM-DD-YYYY');
  v_end_date DATE  := TO_DATE('12-31-2015','MM-DD-YYYY');
  v_new_date DATE  ;
  
BEGIN
    
    FOR rec IN cur_stundent LOOP
       v_new_date:= TO_DATE(TRUNC(DBMS_RANDOM.VALUE(TO_NUMBER(TO_CHAR(v_start_date, 'J')), 
                    TO_NUMBER(TO_CHAR(v_end_date, 'J')))), 'J');
       UPDATE T_STUDENT
        SET
        date_of_birth = v_new_date
        WHERE CURRENT OF cur_stundent;
        --DBMS_OUTPUT.PUT_LINE(v_new_date);
        
    END LOOP;
    COMMIT;
END;